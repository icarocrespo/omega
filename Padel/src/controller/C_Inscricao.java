package controller;

import dao.DuplaDAO;
import dao.EtapaDAO;
import dao.InscricaoDAO;
import dao.JogadorDAO;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Categoria;
import model.Dupla;
import model.Etapa;
import model.Inscricao;
import model.InscricaoPK;
import model.Jogador;

/**
 *
 * @author icaro
 */
public class C_Inscricao implements C_Generic<Inscricao> {

    private Inscricao inscricao;
    private Jogador jogador1;
    private Jogador jogador2;
    private Dupla dupla;
    private Etapa etapa;

    private JogadorDAO jogadorDAO;
    private DuplaDAO duplaDAO;
    private EtapaDAO etapaDAO;
    private InscricaoDAO inscricaoDAO;

    public C_Inscricao() {
        inscricao = new Inscricao();
    }

    private boolean newJogador(Jogador jogador) {
        //String msg; 
        try {
            jogadorDAO = new JogadorDAO();

            if (jogadorDAO.exitsCpf(jogador.getCpf()) == null && ValidaCPF.isCPF(jogador.getCpf())) {
                jogadorDAO.incluir(jogador);
                //       msg = "jogador registrado com sucesso";
                return true;
            }
        } catch (Exception e) {
            System.out.println("Erro ao persistir os jogadores.");
        }
        return false;
    }

    private Dupla newDupla(Jogador jogador1, Jogador jogador2) {
        dupla = new Dupla();

        try {
            dupla.setPontuacao(0);
            if (newJogador(jogador1) && newJogador(jogador2)) {
                dupla.setJogador1(jogador1);
                dupla.setJogador2(jogador2);
            }

            duplaDAO.incluir(dupla);
        } catch (Exception ex) {
            Logger.getLogger(C_Inscricao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return dupla;
    }

    public boolean newInscricao(Jogador jogador1, Jogador jogador2, Categoria categoria, Etapa etapa) {
        try {
            duplaDAO = new DuplaDAO();
            inscricao = new Inscricao();

            InscricaoPK inscricaoPK = new InscricaoPK();
            Categoria[] categorias = Categoria.values();

            if(newDupla(jogador1, jogador2) != null){
                inscricao.setDupla1(dupla);
            }
            inscricaoPK.setDupla(dupla.getId());
            inscricaoPK.setEtapa(etapa.getId());
            inscricaoPK.setCategoria(categoria.ordinal());

            inscricao.setInscricaoPK(inscricaoPK);
            inscricao.setHorario(new Timestamp(System.currentTimeMillis()));
            inscricao.setClassificado(false);

            //dupla = duplaDAO.buscarPorChavePrimaria(idDupla);
            //etapa = etapaDAO.buscarPorChavePrimaria(idEtapa);

            if (save(inscricao)) {
                return true;
            } else {
                return false;
            }

        } catch (Exception ex) {
            Logger.getLogger(C_Inscricao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean save(Inscricao inscricao) {
        inscricaoDAO = new InscricaoDAO();
        boolean status;

        try {
            inscricaoDAO.incluir(inscricao);
            status = true;
        } catch (Exception e) {
            String log = e.getMessage();
            status = false;

        }
        return status;
    }

    @Override
    public boolean remove(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//    public boolean alter(Inscricao inscricao, Integer idEupla, Integer idEtapa) {
//        if(inscricao != null){
//            inscricaoDAO = new InscricaoDAO();
//            inscricaoDAO.buscarPorChavePrimaria(idEtapa);
//        }
//    }
    @Override
    public boolean alter(Inscricao inscricao, Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Inscricao> list(String parameter) {
        List<Inscricao> lista;

        if (!parameter.isEmpty()) {
            inscricaoDAO = new InscricaoDAO();
            try {
                lista = inscricaoDAO.listar(parameter);
                return lista;
            } catch (Exception ex) {
                Logger.getLogger(C_Inscricao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
