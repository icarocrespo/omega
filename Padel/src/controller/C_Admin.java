package controller;

import dao.CircuitoDAO;
import model.Circuito;
import model.Etapa;

/**
 *
 * @author icaro
 */
public class C_Admin {

    private CircuitoDAO circuitoDAO;
    private Circuito circuito;
    private Etapa etapa;

    public C_Admin() {

    }

    public boolean newCircuito(Circuito circuito) {
        try {
            circuitoDAO.incluir(circuito);
            return true;
        } catch (Exception ex) {

        }
        return false;
    }
}