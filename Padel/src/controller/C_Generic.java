package controller;

import java.util.List;

/**
 *
 * @author icaro
 */
public interface C_Generic<T> {

    public boolean save(T o);
    
    public boolean remove(Integer id);
    
    public boolean alter(T o, Integer id);
    
    public List<T> list(String parameter);
}
