package controller;

import dao.ChaveamentoDAO;
import java.util.ArrayList;
import java.util.List;
import model.Chaveamento;
import view.Chaves;

/**
 *
 * @author icaro
 */
public class C_Chaveamento {

    private ChaveamentoDAO chaveamentoDAO;
    private Chaveamento chaveamento;
    private List<Chaveamento> chaveamentos;

    public C_Chaveamento() {

    }

    public List<Chaveamento> showByIdentificador(Chaves chaves) {
        chaveamentoDAO = new ChaveamentoDAO();
        chaveamentos = new ArrayList<>();

        try {
            chaveamentos = chaveamentoDAO.listar(chaves.getChaveamento().getIdentificador());
            return chaveamentos;
        } catch (Exception ex) {

        }
        return null;
    }
}
