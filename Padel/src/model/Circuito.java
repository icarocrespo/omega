/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author icaro
 */
@Entity
@Table(name = "circuito")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Circuito.findAll", query = "SELECT c FROM Circuito c")
    , @NamedQuery(name = "Circuito.findById", query = "SELECT c FROM Circuito c WHERE c.id = :id")
    , @NamedQuery(name = "Circuito.findByNome", query = "SELECT c FROM Circuito c WHERE c.nome = :nome")
    , @NamedQuery(name = "Circuito.findByRegiao", query = "SELECT c FROM Circuito c WHERE c.regiao = :regiao")
    , @NamedQuery(name = "Circuito.findByResponsavel", query = "SELECT c FROM Circuito c WHERE c.responsavel = :responsavel")
    , @NamedQuery(name = "Circuito.findFilter", query = "SELECT c FROM Circuito c WHERE c.responsavel like :filtro")
})
public class Circuito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 50)
    private String nome;
    @Basic(optional = false)
    @Column(name = "regiao", nullable = false, length = 20)
    private String regiao;
    @Basic(optional = false)
    @Column(name = "responsavel", nullable = false, length = 30)
    private String responsavel;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "circuito")
    private List<Etapa> etapaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "circuito")
    private List<Ranking> rankingList;

    public Circuito() {
    }

    public Circuito(Integer id) {
        this.id = id;
    }

    public Circuito(Integer id, String nome, String regiao, String responsavel) {
        this.id = id;
        this.nome = nome;
        this.regiao = regiao;
        this.responsavel = responsavel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRegiao() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        this.regiao = regiao;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    @XmlTransient
    public List<Etapa> getEtapaList() {
        return etapaList;
    }

    public void setEtapaList(List<Etapa> etapaList) {
        this.etapaList = etapaList;
    }

    @XmlTransient
    public List<Ranking> getRankingList() {
        return rankingList;
    }

    public void setRankingList(List<Ranking> rankingList) {
        this.rankingList = rankingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Circuito)) {
            return false;
        }
        Circuito other = (Circuito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Circuito[ id=" + id + " ]";
    }
    
}
