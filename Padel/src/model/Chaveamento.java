/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author icaro
 */
@Entity
@Table(name = "chaveamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Chaveamento.findAll", query = "SELECT c FROM Chaveamento c")
    , @NamedQuery(name = "Chaveamento.findByIdentificador", query = "SELECT c FROM Chaveamento c WHERE c.identificador = :identificador")
    , @NamedQuery(name = "Chaveamento.findByDupla", query = "SELECT c FROM Chaveamento c WHERE c.chaveamentoPK.dupla = :dupla")
    , @NamedQuery(name = "Chaveamento.findByEtapa", query = "SELECT c FROM Chaveamento c WHERE c.chaveamentoPK.etapa = :etapa")
    , @NamedQuery(name = "Chaveamento.findFilter", query = "SELECT c FROM Chaveamento c WHERE c.identificador like :filtro")

})
public class Chaveamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ChaveamentoPK chaveamentoPK;
    @Basic(optional = false)
    @Column(name = "identificador", nullable = false, length = 10)
    private String identificador;
    @JoinColumn(name = "dupla", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Dupla dupla1;
    @JoinColumn(name = "etapa", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Etapa etapa1;

    public Chaveamento() {
    }

    public Chaveamento(ChaveamentoPK chaveamentoPK) {
        this.chaveamentoPK = chaveamentoPK;
    }

    public Chaveamento(ChaveamentoPK chaveamentoPK, String identificador) {
        this.chaveamentoPK = chaveamentoPK;
        this.identificador = identificador;
    }

    public Chaveamento(int dupla, int etapa) {
        this.chaveamentoPK = new ChaveamentoPK(dupla, etapa);
    }

    public ChaveamentoPK getChaveamentoPK() {
        return chaveamentoPK;
    }

    public void setChaveamentoPK(ChaveamentoPK chaveamentoPK) {
        this.chaveamentoPK = chaveamentoPK;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public Dupla getDupla1() {
        return dupla1;
    }

    public void setDupla1(Dupla dupla1) {
        this.dupla1 = dupla1;
    }

    public Etapa getEtapa1() {
        return etapa1;
    }

    public void setEtapa1(Etapa etapa1) {
        this.etapa1 = etapa1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (chaveamentoPK != null ? chaveamentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Chaveamento)) {
            return false;
        }
        Chaveamento other = (Chaveamento) object;
        if ((this.chaveamentoPK == null && other.chaveamentoPK != null) || (this.chaveamentoPK != null && !this.chaveamentoPK.equals(other.chaveamentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Chaveamento[ chaveamentoPK=" + chaveamentoPK + " ]";
    }
    
}
