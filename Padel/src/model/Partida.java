/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author icaro
 */
@Entity
@Table(name = "partida")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Partida.findAll", query = "SELECT p FROM Partida p")
    , @NamedQuery(name = "Partida.findById", query = "SELECT p FROM Partida p WHERE p.id = :id")
    , @NamedQuery(name = "Partida.findByHorario", query = "SELECT p FROM Partida p WHERE p.horario = :horario")
    , @NamedQuery(name = "Partida.findFilter", query = "SELECT p FROM Partida p WHERE p.etapa like :filtro")
})
public class Partida implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "horario", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date horario;
    @JoinColumn(name = "dupla1", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Dupla dupla1;
    @JoinColumn(name = "dupla2", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Dupla dupla2;
    @JoinColumn(name = "etapa", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Etapa etapa;
    @JoinColumn(name = "localizacao", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Localizacao localizacao;

    public Partida() {
    }

    public Partida(Integer id) {
        this.id = id;
    }

    public Partida(Integer id, Date horario) {
        this.id = id;
        this.horario = horario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getHorario() {
        return horario;
    }

    public void setHorario(Date horario) {
        this.horario = horario;
    }

    public Dupla getDupla1() {
        return dupla1;
    }

    public void setDupla1(Dupla dupla1) {
        this.dupla1 = dupla1;
    }

    public Dupla getDupla2() {
        return dupla2;
    }

    public void setDupla2(Dupla dupla2) {
        this.dupla2 = dupla2;
    }

    public Etapa getEtapa() {
        return etapa;
    }

    public void setEtapa(Etapa etapa) {
        this.etapa = etapa;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partida)) {
            return false;
        }
        Partida other = (Partida) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Partida[ id=" + id + " ]";
    }
    
}
