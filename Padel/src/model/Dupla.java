/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author icaro
 */
@Entity
@Table(name = "dupla")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dupla.findAll", query = "SELECT d FROM Dupla d")
    , @NamedQuery(name = "Dupla.findById", query = "SELECT d FROM Dupla d WHERE d.id = :id")
    , @NamedQuery(name = "Dupla.findByPontuacao", query = "SELECT d FROM Dupla d WHERE d.pontuacao = :pontuacao")
    , @NamedQuery(name = "Dupla.findByJogador2", query = "SELECT d FROM Dupla d WHERE d.jogador2 = :jogador2")
    , @NamedQuery(name = "Dupla.findFilter", query = "SELECT d FROM Dupla d WHERE d.pontuacao like :filtro")
})
public class Dupla implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "pontuacao", nullable = false)
    private int pontuacao;
    @JoinColumn(name = "jogador2", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Jogador jogador2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dupla1")
    private List<Chaveamento> chaveamentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dupla1")
    private List<Inscricao> inscricaoList;
    @JoinColumn(name = "jogador1", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Jogador jogador1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dupla1")
    private List<Partida> partidaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dupla2")
    private List<Partida> partidaList1;

    public Dupla() {
    }

    public Dupla(Integer id) {
        this.id = id;
    }

    public Dupla(Integer id, int pontuacao, Jogador jogador2) {
        this.id = id;
        this.pontuacao = pontuacao;
        this.jogador2 = jogador2;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public Jogador getJogador2() {
        return jogador2;
    }

    public void setJogador2(Jogador jogador2) {
        this.jogador2 = jogador2;
    }

    @XmlTransient
    public List<Chaveamento> getChaveamentoList() {
        return chaveamentoList;
    }

    public void setChaveamentoList(List<Chaveamento> chaveamentoList) {
        this.chaveamentoList = chaveamentoList;
    }

    @XmlTransient
    public List<Inscricao> getInscricaoList() {
        return inscricaoList;
    }

    public void setInscricaoList(List<Inscricao> inscricaoList) {
        this.inscricaoList = inscricaoList;
    }

    public Jogador getJogador1() {
        return jogador1;
    }

    public void setJogador1(Jogador jogador1) {
        this.jogador1 = jogador1;
    }

    @XmlTransient
    public List<Partida> getPartidaList() {
        return partidaList;
    }

    public void setPartidaList(List<Partida> partidaList) {
        this.partidaList = partidaList;
    }

    @XmlTransient
    public List<Partida> getPartidaList1() {
        return partidaList1;
    }

    public void setPartidaList1(List<Partida> partidaList1) {
        this.partidaList1 = partidaList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dupla)) {
            return false;
        }
        Dupla other = (Dupla) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Dupla[ id=" + id + " ]";
    }
    
}
