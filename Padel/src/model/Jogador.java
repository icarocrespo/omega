/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author icaro
 */
@Entity
@Table(name = "jogador", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"cpf"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jogador.findAll", query = "SELECT j FROM Jogador j")
    , @NamedQuery(name = "Jogador.findById", query = "SELECT j FROM Jogador j WHERE j.id = :id")
    , @NamedQuery(name = "Jogador.findByNome", query = "SELECT j FROM Jogador j WHERE j.nome = :nome")
    , @NamedQuery(name = "Jogador.findByCpf", query = "SELECT j FROM Jogador j WHERE j.cpf = :cpf")
    , @NamedQuery(name = "Jogador.findByEmail", query = "SELECT j FROM Jogador j WHERE j.email = :email")
    , @NamedQuery(name = "Jogador.findBySexo", query = "SELECT j FROM Jogador j WHERE j.sexo = :sexo")
    , @NamedQuery(name = "Jogador.findByPontuacao", query = "SELECT j FROM Jogador j WHERE j.pontuacao = :pontuacao")
    , @NamedQuery(name = "Jogador.findByTelefone", query = "SELECT j FROM Jogador j WHERE j.telefone = :telefone")
    , @NamedQuery(name = "Jgador.findFilter", query = "SELECT j FROM Jogador j WHERE j.pontuacao like :filtro")
})
public class Jogador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 50)
    private String nome;
    @Basic(optional = false)
    @Column(name = "cpf", nullable = false, length = 11)
    private String cpf;
    @Basic(optional = false)
    @Column(name = "email", nullable = false, length = 30)
    private String email;
    @Basic(optional = false)
    @Column(name = "sexo", nullable = false)
    private int sexo;
    @Basic(optional = false)
    @Column(name = "pontuacao", nullable = false)
    private int pontuacao;
    @Basic(optional = false)
    @Column(name = "telefone", nullable = false, length = 45)
    private String telefone;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jogador1")
    private List<Ranking> rankingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jogador1")
    private List<Dupla> duplaList;

    public Jogador() {
    }

    public Jogador(Integer id) {
        this.id = id;
    }

    public Jogador(Integer id, String nome, String cpf, String email, int sexo, int pontuacao, String telefone) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.sexo = sexo;
        this.pontuacao = pontuacao;
        this.telefone = telefone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @XmlTransient
    public List<Ranking> getRankingList() {
        return rankingList;
    }

    public void setRankingList(List<Ranking> rankingList) {
        this.rankingList = rankingList;
    }

    @XmlTransient
    public List<Dupla> getDuplaList() {
        return duplaList;
    }

    public void setDuplaList(List<Dupla> duplaList) {
        this.duplaList = duplaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jogador)) {
            return false;
        }
        Jogador other = (Jogador) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Jogador[ id=" + id + " ]";
    }
    
}
