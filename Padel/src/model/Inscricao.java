/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author icaro
 */
@Entity
@Table(name = "inscricao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inscricao.findAll", query = "SELECT i FROM Inscricao i")
    , @NamedQuery(name = "Inscricao.findByHorario", query = "SELECT i FROM Inscricao i WHERE i.horario = :horario")
    , @NamedQuery(name = "Inscricao.findByClassificado", query = "SELECT i FROM Inscricao i WHERE i.classificado = :classificado")
    , @NamedQuery(name = "Inscricao.findByCategoria", query = "SELECT i FROM Inscricao i WHERE i.inscricaoPK.categoria = :categoria")
    , @NamedQuery(name = "Inscricao.findByDupla", query = "SELECT i FROM Inscricao i WHERE i.inscricaoPK.dupla = :dupla")
    , @NamedQuery(name = "Inscricao.findByEtapa", query = "SELECT i FROM Inscricao i WHERE i.inscricaoPK.etapa = :etapa")
    , @NamedQuery(name = "Inscricao.findFilter", query = "SELECT i FROM Inscricao i WHERE i.classificado like :filtro")
    , @NamedQuery(name = "Inscricao.classificados", query = "SELECT i FROM Inscricao i WHERE i.classificado = true AND I.etapa1 like :idEtapa")
})

public class Inscricao implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InscricaoPK inscricaoPK;
    @Basic(optional = false)
    @Column(name = "horario", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date horario;
    @Basic(optional = false)
    @Column(name = "classificado", nullable = false)
    private boolean classificado;
    @JoinColumn(name = "dupla", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Dupla dupla1;
    @JoinColumn(name = "etapa", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Etapa etapa1;

    public Inscricao() {
    }

    public Inscricao(InscricaoPK inscricaoPK) {
        this.inscricaoPK = inscricaoPK;
    }

    public Inscricao(InscricaoPK inscricaoPK, Date horario, boolean classificado) {
        this.inscricaoPK = inscricaoPK;
        this.horario = horario;
        this.classificado = classificado;
    }

    public Inscricao(int categoria, int dupla, int etapa) {
        this.inscricaoPK = new InscricaoPK(categoria, dupla, etapa);
    }

    public InscricaoPK getInscricaoPK() {
        return inscricaoPK;
    }

    public void setInscricaoPK(InscricaoPK inscricaoPK) {
        this.inscricaoPK = inscricaoPK;
    }

    public Date getHorario() {
        return horario;
    }

    public void setHorario(Date horario) {
        this.horario = horario;
    }

    public boolean getClassificado() {
        return classificado;
    }

    public void setClassificado(boolean classificado) {
        this.classificado = classificado;
    }

    public Dupla getDupla1() {
        return dupla1;
    }

    public void setDupla1(Dupla dupla1) {
        this.dupla1 = dupla1;
    }

    public Etapa getEtapa1() {
        return etapa1;
    }

    public void setEtapa1(Etapa etapa1) {
        this.etapa1 = etapa1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inscricaoPK != null ? inscricaoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inscricao)) {
            return false;
        }
        Inscricao other = (Inscricao) object;
        if ((this.inscricaoPK == null && other.inscricaoPK != null) || (this.inscricaoPK != null && !this.inscricaoPK.equals(other.inscricaoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Inscricao[ inscricaoPK=" + inscricaoPK + " ]";
    }
    
}
