/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author icaro
 */
@Entity
@Table(name = "ranking")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ranking.findAll", query = "SELECT r FROM Ranking r")
    , @NamedQuery(name = "Ranking.findByJogador", query = "SELECT r FROM Ranking r WHERE r.rankingPK.jogador = :jogador")
    , @NamedQuery(name = "Ranking.findByPontuacao", query = "SELECT r FROM Ranking r WHERE r.rankingPK.pontuacao = :pontuacao")})

@NamedNativeQueries({
    @NamedNativeQuery(name = "Ranking.orderDecres", query = "SELECT * FROM Ranking ORDER BY pontuacao",
        resultClass = Ranking.class)
})
public class Ranking implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RankingPK rankingPK;
    @JoinColumn(name = "circuito", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Circuito circuito;
    @JoinColumn(name = "jogador", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Jogador jogador1;

    public Ranking() {
    }

    public Ranking(RankingPK rankingPK) {
        this.rankingPK = rankingPK;
    }

    public Ranking(int jogador, int pontuacao) {
        this.rankingPK = new RankingPK(jogador, pontuacao);
    }

    public RankingPK getRankingPK() {
        return rankingPK;
    }

    public void setRankingPK(RankingPK rankingPK) {
        this.rankingPK = rankingPK;
    }

    public Circuito getCircuito() {
        return circuito;
    }

    public void setCircuito(Circuito circuito) {
        this.circuito = circuito;
    }

    public Jogador getJogador1() {
        return jogador1;
    }

    public void setJogador1(Jogador jogador1) {
        this.jogador1 = jogador1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rankingPK != null ? rankingPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ranking)) {
            return false;
        }
        Ranking other = (Ranking) object;
        if ((this.rankingPK == null && other.rankingPK != null) || (this.rankingPK != null && !this.rankingPK.equals(other.rankingPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Ranking[ rankingPK=" + rankingPK + " ]";
    }
    
}
