/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author icaro
 */
@Entity
@Table(name = "etapa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Etapa.findAll", query = "SELECT e FROM Etapa e")
    , @NamedQuery(name = "Etapa.findById", query = "SELECT e FROM Etapa e WHERE e.id = :id")
    , @NamedQuery(name = "Etapa.findByNome", query = "SELECT e FROM Etapa e WHERE e.nome = :nome")
    , @NamedQuery(name = "Etapa.findByVencedor", query = "SELECT e FROM Etapa e WHERE e.vencedor = :vencedor")
    , @NamedQuery(name = "Etapa.findFilter", query = "SELECT e FROM Etapa e WHERE e.nome like :filtro")
})
public class Etapa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 50)
    private String nome;
    @Basic(optional = false)
    @Column(name = "vencedor", nullable = false)
    private int vencedor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "etapa1")
    private List<Chaveamento> chaveamentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "etapa1")
    private List<Inscricao> inscricaoList;
    @JoinColumn(name = "circuito", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Circuito circuito;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "etapa")
    private List<Partida> partidaList;

    public Etapa() {
    }

    public Etapa(Integer id) {
        this.id = id;
    }

    public Etapa(Integer id, String nome, int vencedor) {
        this.id = id;
        this.nome = nome;
        this.vencedor = vencedor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getVencedor() {
        return vencedor;
    }

    public void setVencedor(int vencedor) {
        this.vencedor = vencedor;
    }

    @XmlTransient
    public List<Chaveamento> getChaveamentoList() {
        return chaveamentoList;
    }

    public void setChaveamentoList(List<Chaveamento> chaveamentoList) {
        this.chaveamentoList = chaveamentoList;
    }

    @XmlTransient
    public List<Inscricao> getInscricaoList() {
        return inscricaoList;
    }

    public void setInscricaoList(List<Inscricao> inscricaoList) {
        this.inscricaoList = inscricaoList;
    }

    public Circuito getCircuito() {
        return circuito;
    }

    public void setCircuito(Circuito circuito) {
        this.circuito = circuito;
    }

    @XmlTransient
    public List<Partida> getPartidaList() {
        return partidaList;
    }

    public void setPartidaList(List<Partida> partidaList) {
        this.partidaList = partidaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Etapa)) {
            return false;
        }
        Etapa other = (Etapa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Etapa[ id=" + id + " ]";
    }
    
}
