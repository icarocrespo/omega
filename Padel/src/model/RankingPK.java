/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author icaro
 */
@Embeddable
public class RankingPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "jogador", nullable = false)
    private int jogador;
    @Basic(optional = false)
    @Column(name = "pontuacao", nullable = false)
    private int pontuacao;

    public RankingPK() {
    }

    public RankingPK(int jogador, int pontuacao) {
        this.jogador = jogador;
        this.pontuacao = pontuacao;
    }

    public int getJogador() {
        return jogador;
    }

    public void setJogador(int jogador) {
        this.jogador = jogador;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) jogador;
        hash += (int) pontuacao;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RankingPK)) {
            return false;
        }
        RankingPK other = (RankingPK) object;
        if (this.jogador != other.jogador) {
            return false;
        }
        if (this.pontuacao != other.pontuacao) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.RankingPK[ jogador=" + jogador + ", pontuacao=" + pontuacao + " ]";
    }
    
}
