/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author icaro
 */
@Embeddable
public class InscricaoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "categoria", nullable = false)
    private int categoria;
    @Basic(optional = false)
    @Column(name = "dupla", nullable = false)
    private int dupla;
    @Basic(optional = false)
    @Column(name = "etapa", nullable = false)
    private int etapa;

    public InscricaoPK() {
    }

    public InscricaoPK(int categoria, int dupla, int etapa) {
        this.categoria = categoria;
        this.dupla = dupla;
        this.etapa = etapa;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public int getDupla() {
        return dupla;
    }

    public void setDupla(int dupla) {
        this.dupla = dupla;
    }

    public int getEtapa() {
        return etapa;
    }

    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) categoria;
        hash += (int) dupla;
        hash += (int) etapa;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InscricaoPK)) {
            return false;
        }
        InscricaoPK other = (InscricaoPK) object;
        if (this.categoria != other.categoria) {
            return false;
        }
        if (this.dupla != other.dupla) {
            return false;
        }
        if (this.etapa != other.etapa) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.InscricaoPK[ categoria=" + categoria + ", dupla=" + dupla + ", etapa=" + etapa + " ]";
    }
    
}
