package model;

/**
 *
 * @author icaro
 */
public enum Categoria {
    MASCULINO_1, MASCULINO_2, MASCULINO_3, FEMININO_1, FEMININO_2, FEMININO_3, INFANTIL
}
