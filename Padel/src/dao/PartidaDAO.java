package dao;

import model.Partida;

/**
 *
 * @author icaro
 */
public class PartidaDAO extends GenericDAO<Partida, Integer>{
    
    public PartidaDAO(){
        super(Partida.class);
}
    
}
