package dao;

import java.util.List;
import model.Chaveamento;

/**
 *
 * @author icaro
 */
public class ChaveamentoDAO extends GenericDAO<Chaveamento, Integer> {

    public ChaveamentoDAO() {
        super(Chaveamento.class);
    }
    
    public List<Chaveamento> findByIdentificador(String id, String etapa, String dupla) throws Exception {
        return em.createNamedQuery("Chaveamento.findByIdentificador").setParameter("filtro", "%" + id + "%").getResultList();
    }
}
