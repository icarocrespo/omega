package dao;

import java.util.List;
import model.Inscricao;

/**
 *
 * @author icaro
 */
public class InscricaoDAO extends GenericDAO<Inscricao, Integer>{
    
    public InscricaoDAO() {
        super(Inscricao.class);
    }
    
    public List<Inscricao> listarClassificados(Integer idEtapa) throws Exception {
        return em.createNamedQuery("Inscricao.classificados").setParameter("idEtapa", "%" + idEtapa + "%").getResultList();
    }
}
