package dao;

import java.util.List;
import model.Ranking;

/**
 *
 * @author icaro
 */
public class RankingDAO extends GenericDAO<Ranking, Integer>{
    
    public RankingDAO(){
        super(Ranking.class);
}
    public List<Ranking> listarOrdenado() throws Exception {
        return em.createNamedQuery("Ranking.orderDecres").getResultList();
    }
}
