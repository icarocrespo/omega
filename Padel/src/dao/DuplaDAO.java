package dao;

import model.Dupla;

/**
 *
 * @author icaro
 */
public class DuplaDAO extends GenericDAO<Dupla, Integer> {

    public DuplaDAO() {
        super(Dupla.class);
    }

}
