package dao;

import model.Jogador;

/**
 *
 * @author icaro
 */
public class JogadorDAO extends GenericDAO<Jogador, Integer> {

    public JogadorDAO() {
        super(Jogador.class);
    }

    public Jogador exitsCpf(String cpf) {
        return (Jogador) em.createNamedQuery("Jogador.findByCpf").setParameter("cpf", "%" + cpf + "%").getSingleResult();
    }

}
