package dao;

import model.Localizacao;

/**
 *
 * @author icaro
 */
public class LocalizacaoDAO extends GenericDAO<Localizacao, Integer>{
    
    public LocalizacaoDAO(){
        super(Localizacao.class);
}
    
}
