package dao;

import model.Circuito;

/**
 *
 * @author icaro
 */
public class CircuitoDAO extends GenericDAO<Circuito, Integer>{
    
    public CircuitoDAO(){
        super(Circuito.class);
}
}
