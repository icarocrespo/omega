package dao;

import model.Etapa;

/**
 *
 * @author icaro
 */
public class EtapaDAO extends GenericDAO<Etapa, Integer>{
    
    public EtapaDAO(){
        super(Etapa.class);
}
    
}
