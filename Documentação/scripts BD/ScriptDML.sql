﻿insert into CATEGORIA values
(1),
(2),
(3),
(4);

insert into LOCALIZACAO values
(1, 'Padelhouse'),
(2, 'Padel Point'),
(3, 'Padel Alegrete');

insert into JOGADOR values
(1, 'João Victor', 'joaovictor@gmail.com', 1, 150, '(55)99664-8859', 1),
(2, 'Icaro', 'icaro@gmail.com', 1, 120, '(55)99864-3454', 1),
(3, 'Francisco', 'francisco@gmail.com', 1, 100, '(51)99964-8349', 1),
(4, 'Maria', 'maria@gmail.com', 2, 155, '(53)99344-8459', 1),
(5, 'Andressa', 'andressa@gmail.com', 2, 80, '(47)99544-8469', 1),
(6, 'Mariele', 'mariele@gmail.com', 2, 60, '(55)99664-8569', 1),
(7, 'João Gabriel', 'joaog@gmail.com', 1, 50, '(51)99644-8479', 1),
(8, 'João Pedro', 'joaop@gmail.com', 1, 54, '(53)99434-8769', 1),
(9, 'Guilherme', 'guilherme@gmail.com', 1, 15, '(55)99454-8769', 1),
(10, 'Paulo', 'paulo@gmail.com', 1, 10, '(55)99664-7659', 1),
(11, 'Davi', 'davi@gmail.com', 1, 145, '(55)99665-4879', 1),
(12, 'Martin', 'martin@gmail.com', 1, 127, '(55)99864-3454', 1),
(13, 'Victor', 'victor@gmail.com', 1, 106, '(51)99964-8349', 1),
(14, 'Victoria', 'victoria@gmail.com', 2, 145, '(53)99344-8459', 1),
(15, 'Alessandra', 'alessandra@gmail.com', 2, 80, '(47)99544-8469', 1),
(16, 'Marie', 'marie@gmail.com', 2, 60, '(55)99664-8569', 1),
(17, 'José', 'jose@gmail.com', 1, 50, '(51)99644-8479', 1),
(18, 'José Pedro', 'josep@gmail.com', 1, 54, '(53)99434-8769', 1),
(19, 'antonio', 'antonio@gmail.com', 1, 15, '(55)99454-8769', 1),
(20, 'marcelo', 'marcelo@gmail.com', 1, 10, '(55)99664-7659', 1);

insert into CIRCUITO values
(1, 'Circuito Brasileiro de Padel', 'Brasil', 1),
(1, 'Circuito Gaúcho de Padel', 'RS', 1),

insert into ETAPA values
(1, 'Etapa Sul/Sudeste', 5, 1),
(2, 'Etapa Norte/Nordeste', 7, 1),
(3, 'Etapa Central', 3, 1),
(4, 'Etapa Fronteira Oeste', 5, 2),
(5, 'Etapa Capital', 5, 2),
(6, 'Etapa Serra', 5, 2);

insert into DUPLA values
(1, 160, 1, 10, 4),
(2, 135, 2, 9, 4),
(3, 154, 3, 8, 4),
(4, 205, 4, 7, 4),
(5, 140, 5, 6, 4),
(6, 155, 11, 20, 4),
(7, 142, 12, 19, 4),
(8, 160, 13, 18, 4),
(9, 195, 14, 17, 4),
(10, 140, 15, 16, 4);

insert into PARTIDA values
(1, '2019-09-03 17:00:00', 1, 2, 4),
(1, '2019-09-03 19:00:00', 3, 4, 4),
(1, '2019-09-04 17:00:00', 5, 6, 4);

insert into IMPEDIMENTOS values

